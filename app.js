'use strict';

const express = require('express'),
    fs = require('fs'),
    passport = require('passport'),
    configRoutes = require('./routes/routesConfig.js').loadRoutes,
    mongoose = require('mongoose'),
    morgan = require('morgan'),
    authenticateRoute = require('./authConfig.js'),
    app = express(),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    accessLogStream = fs.createWriteStream(__dirname + '/httpLog.log', {flags: 'a'}),
    User = require('./models/user.js'),
    Item = require('./models/item.js'),
    Post = require('./models/post.js'),
    Comment = require('./models/comment.js'),
    appEvents = require('./applicationEvents.js');
    //io = require('socket.io').listen(app.listen(3000));



app.set('views', './views');
app.set('view engine', 'pug');

app.use('/public', express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));


//CORS
var cors = 'https://brogg.herokuapp.com';
console.log(cors);
var allowCrossDomain = function(req, res, next) {
    if ('OPTIONS' == req.method) {
      res.header('Access-Control-Allow-Origin', cors);
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
      res.header("Access-Control-Allow-Credentials","true");
      console.log("accepting options cors request");
      return res.sendStatus(200).end();
    }
    else if ('POST'== req.method){
      res.header('Access-Control-Allow-Origin', cors);
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With', 'Accept');
      res.header("Access-Control-Allow-Credentials","true");
    }
    else if ('GET'== req.method){
      res.header('Access-Control-Allow-Origin', cors);
      res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
      res.header('Access-Control-Allow-Headers', 'Authorization, Content-Length, X-Requested-With', 'Accept');
      res.header("Access-Control-Allow-Credentials","true");
    }
      next();
};
app.use(allowCrossDomain);
console.log(cors + "set as allowed");
// app.use(function(req, res, next) {
//   res.header("Access-Control-Allow-Origin", "http://localhost:4200");
//   res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   res.header("Access-Control-Allow-Credentials","true");

//   next();
// });

app.use(session({ secret: 'what cat' }));
app.use(passport.initialize());
app.use(passport.session());

app.use(authenticateRoute);

app.use(morgan("combined", { timestamp: true, stream: accessLogStream }));

//mongoose.connect('mongodb://localhost/test');
mongoose.connect('mongodb://heroku_9g8r0ldt:a3fccuefba93d2ql1j8qd45kes@ds119568.mlab.com:19568/heroku_9g8r0ldt');
mongoose.Promise = global.Promise = require('bluebird');

passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());




configRoutes(app);
appEvents.triggerAppReady();

let defaultUsername = "nikolay.tsvetanov";

//SEEDING ADMIN USER
User.findOne({ username: defaultUsername }, function (err, user) {
  if(err) { console.log(err) }
  if (!user) { 
    let newUser = new User({ username: "nikolay.tsvetanov", companyId: "Company1", role: "Admin"});
    
    User.register(newUser, 
        "123", 
        (err, account) => {
            if(err) {
                console.log(err);
            }
        });
    console.log('Seeding user.');
   }
});


// crudUser.create(user);
// crudPost = require('../utilities/generateCrudOperations.js')(Post);



//require('./services/socketService.js').init(io);
app.listen(process.env.PORT || 3000);
console.log('ready and listening on port' + process.env.PORT || 3000);















