'use strict';

let itemService = require('../services/itemService.js'),
    c = require("bluebird").coroutine;

module.exports = function receiverRoutes(app) {
    app.get('/item/all', c(function *(req, res) {
        let items = yield itemService.getItems();

        res.send(items);
    }));

    app.post('/item/create', c(function *(req, res) {
        let itemData = req.body;

        let item = yield itemService.create(itemData);
        
        res.send(200);
    }));

    app.post('/item/edit', c(function *(req, res) {
        let itemData = req.body;

        let item = yield itemService.edit(itemData);
        
        res.send(200);
    }));

    app.post('/item/delete', c(function *(req, res) {
        let itemId = req.body._id;

        let item = yield itemService.delete(itemId);
        
        res.send(200);
    }));
};
