'use strict';

module.exports = function templatesRoutes(app) {
    app.get('/templates/*', function (req, res) {
        let templateUrl = req.originalUrl.replace('/templates/', '');
        res.render(templateUrl);
    });
};