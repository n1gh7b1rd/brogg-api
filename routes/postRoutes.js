'use strict';

let postService = require('../services/postService.js'),
    c = require("bluebird").coroutine;

module.exports = function postRoutes(app) {
    app.get('/post/all', c(function *(req, res) {
        let posts = yield postService.getAll();

        res.send(posts);
    }));

    app.get('/post/getById/:id/', c(function *(req, res) {
        let postId = req.params.id;
        let post = yield postService.getAll({_id: postId});
        res.send(post[0]);
    }));

    
    app.post('/post/create', c(function *(req, res) {
        let postData = req.body;

        let post = yield postService.create(postData);
        
        res.send(200);
    }));

    app.post('/post/edit', c(function *(req, res) {
        let postData = req.body;

        let post = yield postService.edit(postData);
        
        res.send(200);
    }));

    app.post('/post/delete', c(function *(req, res) {
        let postId = req.body._id;

        let post = yield postService.delete(postId);
        
        res.send(200);
    }));
};