'use strict';

let commentService = require('../services/commentService.js'),
    c = require("bluebird").coroutine;
let userService = require('../services/userService.js');

module.exports = function commentRoutes(app) {

    app.get('/comment/getByPostId/:id/', c(function *(req, res) {
        let postId = req.params.id;
        let comments = yield commentService.getAll({post: postId});
        if(comments.length>0){
            res.send(comments);
        }else{
            res.send([]);
        }
    }));

    
    app.post('/comment/add', c(function *(req, res) {
        let newComment = {};
        let user = yield userService.getAll({_id: req.user._id});
        newComment.user = req.user._id;
        newComment.text = req.body.text;
        newComment.post = req.body.postId;
        newComment.author = user[0].username;
        let comment = yield commentService.create(newComment);
        
        res.send(200);
    }));

    app.post('/comment/edit', c(function *(req, res) {
        let postData = req.body;

        let comment = yield commentService.edit(postData);
        
        res.send(200);
    }));

    app.post('/comment/delete', c(function *(req, res) {
        let commentId = req.body._id;

        let comment = yield commentService.delete(commentId);
        
        res.send(200);
    }));
};