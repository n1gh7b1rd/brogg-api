'use strict';

let itemService = require('../services/itemService.js');

module.exports = function receiverRoutes(app) {
    app.get('/receiver/:sensorId/:value', function (req, res) {
        let sensorId = req.params.sensorId,
            value = req.params.value;

        itemService.updateSensorStateAndEmit(sensorId, value);
        
        res.sendStatus(200);
    });
};
