'use strict';

module.exports = function defaultRoute(app) {
    app.get('/', (req, res) => {
        res.redirect('login');
    });

    app.get('/favicon.ico', (req, res) => {
        res.writeHead(200, { 'Content-Type': 'image/x-icon' });
        res.end();
    });
};
