'use strict';

let fs = require('fs'),
    routesPath = __dirname + '/',
    path = require('path'),
    thisFileName = path.basename(__filename);

module.exports = {
    loadRoutes: function (app) {
        let fileNames = fs.readdirSync(routesPath);

        fileNames.forEach(function(fileName) {
            if(fileName === thisFileName) return;

            let pathToFile = path.normalize(routesPath + fileName);
            require(pathToFile)(app);
        });
    }
};
