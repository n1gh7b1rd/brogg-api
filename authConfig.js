'use strict';

module.exports = authenticateRoute;
var excludedRoutes = ['/login', '/register', '/logout', '/post/all'];
var _ = require('lodash');

function authenticateRoute(req, res, next) {
    if(req.method=='OPTIONS') { next(); return; }

    var isExcluded = _.includes(excludedRoutes, req.url);
    var staticFiles = "/public";
    if(req.url.indexOf("/public")==0 || req.url.indexOf("/favicon.ico")==0)
    {
        next();
        return;
    }
    else{
        if (!isExcluded){
            return authenticate(req, res, next);
        }

        next();
        return;
    }
}

function authenticate(req, res, next) {
    if(!req.user)
        return res.sendStatus(401).end();

    next();
}
