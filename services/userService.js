'use strict';

let _ = require('lodash'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    crudOperations = require('../utilities/generateCrudOperations.js')(User),
    userService = {};

userService.create = crudOperations.create;
userService.edit = crudOperations.edit;
userService.delete = crudOperations.delete;
userService.getAll = getAll;


module.exports = userService;

function getAll(query) {
    let users = User.find(query || {})
        .lean()
        .exec();

    return users;
}