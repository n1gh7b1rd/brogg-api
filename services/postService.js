'use strict';

let _ = require('lodash'),
    mongoose = require('mongoose'),
    Post = mongoose.model('Post'),
    crudOperations = require('../utilities/generateCrudOperations.js')(Post),
    postService = {};

postService.create = crudOperations.create;
postService.edit = crudOperations.edit;
postService.delete = crudOperations.delete;
postService.getAll = getAll;


module.exports = postService;

function getAll(query) {
    let posts = Post.find(query || {})
        .lean()
        .exec();

    return posts;
}
