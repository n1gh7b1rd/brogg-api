'use strict';

let _ = require('lodash'),
    mongoose = require('mongoose'),
    Comment = mongoose.model('Comment'),
    crudOperations = require('../utilities/generateCrudOperations.js')(Comment),
    commentService = {};

commentService.create = crudOperations.create;
commentService.edit = crudOperations.edit;
commentService.delete = crudOperations.delete;
commentService.getAll = getAll;


module.exports = commentService;

function getAll(query) {
    let comments = Comment.find(query || {})
        .lean()
        .exec();

    return comments;
}
