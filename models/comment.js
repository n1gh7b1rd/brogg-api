const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    CommentModel = {
        text: String,
        post: {ref: 'PostSchema', type: Schema.Types.ObjectId},
        user: {ref: 'UserSchema', type: Schema.Types.ObjectId},
        author: String,
    },
    CommentSchema = new Schema(CommentModel);

module.exports = mongoose.model('Comment', CommentSchema);