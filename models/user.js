var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    UserModel = {
        companyId: String,
        role: String,
    },
    UserSchema = new Schema(UserModel),
    passportLocalMongoose = require('passport-local-mongoose');

UserSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model('User', UserSchema);
