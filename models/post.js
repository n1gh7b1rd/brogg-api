const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    PostModel = {
        title: String,
        description: String,
        body: String,
        user: {ref: 'UserSchema', type: Schema.Types.ObjectId}
    },
    PostSchema = new Schema(PostModel);

module.exports = mongoose.model('Post', PostSchema);